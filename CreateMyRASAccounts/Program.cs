﻿using System.Data.SqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks.Dataflow;

namespace CreateMyRASAccounts
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["RASPortal"].ConnectionString);
            SqlDataReader sqlDataReader = null;
            try
            {
                sqlCon.Open();
                SqlCommand sqlComm = new SqlCommand("SELECT *,SUBSTRING(CONVERT(varchar(255), NEWID()), 1,3) + '-' + SUBSTRING(CONVERT(varchar(255), NEWID()), 1,3) + '-' + SUBSTRING(CONVERT(varchar(255), NEWID()), 1,3) as Password  FROM dbo.autoCreateAccount", sqlCon);
                sqlComm.CommandTimeout = 300;

                sqlDataReader = sqlComm.ExecuteReader();

                DataTable dTable = new DataTable("NewAccounts");
                dTable.Load(sqlDataReader);

                foreach (DataRow userData in dTable.Rows)
                {
                    CreateSendEmail(userData);
                    
                    using (SqlCommand cmd = new SqlCommand("portal_AutoUserInsert", sqlCon))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@firstName", SqlDbType.VarChar).Value = userData.ItemArray[0].ToString();
                        cmd.Parameters.Add("@lastName", SqlDbType.VarChar).Value = userData.ItemArray[1].ToString();
                        cmd.Parameters.Add("@emailAddress", SqlDbType.VarChar).Value = userData.ItemArray[2].ToString();
                        cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = userData.ItemArray[3].ToString();

                        cmd.ExecuteNonQuery();
                    }
                    System.Threading.Thread.Sleep(1000);
                }
                    

            }
            finally
            {
                sqlDataReader.Close();
                sqlCon.Close();
            }

        }

        private static void CreateSendEmail(DataRow userData)
        {
            using (var smtp = new SmtpClient())
            {
                //smtp.Credentials = new NetworkCredential("kwon@riskadmin.com", "");
                
                smtp.Host = "mail.riskadmin.com";
                //string emailAddress = "ReihnRN@gmail.com"; 
                string emailAddress = userData.ItemArray[2].ToString();

                MailMessage userLoginEmail = new MailMessage("NoReply@rascompanies.com", emailAddress) //"16053611106@faxmaker.com" 
                {
                    Subject = userData.ItemArray[2].ToString() + " myRAS Credendials",
                    IsBodyHtml = true,
                    Body = "Login Name: " + userData.ItemArray[2].ToString() + " <br /> <br /> Password: " + userData.ItemArray[3].ToString() + "<br /> <br />"
                    //"<br /> <br /> The Password will be sent in a follow up email." + "<br /> <br />"
                };

                MailMessage userPassEmail = new MailMessage("NoReply@rascompanies.com", emailAddress) //"16053611106@faxmaker.com" 
                {
                    Subject = userData.ItemArray[2].ToString() +  " myRAS Credendials",
                    IsBodyHtml = true,
                    Body = "Password: " + userData.ItemArray[3].ToString() + "<br /> <br />"
                };

                //userLoginEmail.Bcc.Add(emailAddress1);
                //userPassEmail.CC.Add(emailAddress1);
   
                try
                {
                    smtp.Send(userLoginEmail);
                    //smtp.Send(userPassEmail);
                }
                catch (System.Net.Mail.SmtpFailedRecipientException exRecipient)
                {

                }
            }
        }
    }

}
